#include "../Inc/state_machine.h"
#include <xc.h>
#include "../Inc/pic32_uart.h"
#include "../Inc/led.h"

#define TIMEOUT_RESET_VALUE 6000

uint8_t single_game_decision_matrix[3][3];

uint16_t player1_answer;
uint16_t player2_answer;

sm_states_t (*sm_callback_array[state_last][event_last])(sm_struct_t*);

sm_struct_t sm;

inline uint8_t sm_set_event(sm_struct_t *sm, sm_events_t event){
    sm->event = event;
    sm->new_event = 1;
}

inline void sm_decrease_counter(sm_struct_t *sm){
    if (sm->timeout_counter)   sm->timeout_counter--;
    
    if (!sm->timeout_counter && sm->timeout_counting_flag){
        sm->timeout_counting_flag = TIMEOUT_COUNTING_OFF;
        switch (sm->state) {
            case state_waiting_answers:  
                sm_set_event(sm, event_answers_timeout_expired);
                break;
            case state_showing_set_waiting:
                sm_set_event(sm, event_showing_set_timeout_expired);
                break;
            case state_showing_match_waiting:
                sm_set_event(sm, event_showing_match_timeout_expired);
                break;
            default:
                break;
        }  
    }  
}

//OK
sm_states_t on_boot_ok(sm_struct_t *sm){
    LED_setup();
    LED_all_off();
    return state_match_idle;
}

//OK
sm_states_t on_start_match_received(sm_struct_t *sm){
    PRINT_MESSAGE_SIMPLE_U1("Match has started!!!\n\n");
    sm->current_set = 0;
    sm->player1_victories = 0;
    sm->player2_victories = 0;
    sm->match_winner      = 0;
    return state_set_idle;
}

//OK
sm_states_t on_button_pressed(sm_struct_t *sm){
    // Activar el timmer
    PRINT_MESSAGE_U1("Set %d has started!!!\n", ++ sm->current_set);
    sm->timeout_counting_flag = TIMEOUT_COUNTING_ON;
    sm->timeout_counter = TIMEOUT_RESET_VALUE;
    return state_waiting_answers;
}
//OK
sm_states_t on_answers_timeout_expired(sm_struct_t *sm){
    // Enviar mensaje via UART
    PRINT_MESSAGE_SIMPLE_U1("Expired Timeout. Players do not want to play ...\n");
    PRINT_MESSAGE_SIMPLE_U1("Ready for the next match? ...\n\n");
    return state_match_idle;
}
//OK
uint8_t is_valid_answer(uint8_t answer){
    return (answer == PAPER || answer == ROCK || answer == SCISSORS);
}
//OK
sm_states_t on_answers_received(sm_struct_t *sm){  
   sscanf(U2RxBuf,"%4hu",&player1_answer);
   sscanf(U3RxBuf,"%4hu",&player2_answer);
   
   if (is_valid_answer(player1_answer) && is_valid_answer(player2_answer)){
       sm->current_set_winner = single_game_decision_matrix[player1_answer][player2_answer];
       switch (single_game_decision_matrix[player1_answer][player2_answer]){
           case 1:
               sm->player1_victories++;
               break;
           case 2:
               sm->player2_victories++;
               break;
           default:
               break;        
       }
       if (sm->current_set >= MAXIMUM_SETS && (sm->player1_victories-sm->player2_victories != 0)){
           sm_set_event(sm, event_match_winner_ok);
       }
       else
           sm_set_event(sm, event_set_result_ok);
   }
   else{
       PRINT_MESSAGE_SIMPLE_U1("Players do not know how to play ...\n");
       PRINT_MESSAGE_SIMPLE_U1("Let's repeat the set!!!\n");
       sm_set_event(sm,event_boot_ok); //// CREAR EVENTOOO
   }
   return state_set_decision;
}
//OK
sm_states_t on_set_result_ok(sm_struct_t *sm){
    switch (sm->current_set_winner){
        case 0:
            PRINT_MESSAGE_SIMPLE_U1("Set result .... TIE \n\n");
            LED_on(LED3);
            break;
        case 1:
            PRINT_MESSAGE_SIMPLE_U1("Player 1 wins the set!!!\n\n");
            LED_on(LED1);
            break;
        case 2:
            PRINT_MESSAGE_SIMPLE_U1("Player 2 wins the set!!!\n\n");
            LED_on(LED2);
            break;
        default:
            break; 
    }      
    
    sm->timeout_counting_flag = TIMEOUT_COUNTING_ON;
    sm->timeout_counter = TIMEOUT_RESET_VALUE;
    return state_showing_set_waiting;
}
//OK
sm_states_t on_showing_set_timeout_expired(sm_struct_t *sm){
    LED_all_off();
    PRINT_MESSAGE_SIMPLE_U1("Ready for the next set???\n");
    return state_set_idle;
}
//OK
sm_states_t on_match_winner_ok(sm_struct_t *sm){
    if (sm->player1_victories > sm->player2_victories){
        PRINT_MESSAGE_SIMPLE_U1("Player1 wins the match!!!\n\n");
        LED_on(LED1);
    }
    else{
        PRINT_MESSAGE_SIMPLE_U1("Player2 wins the match!!!\n\n");
        LED_on(LED2);
    }
    LED_on(LED3);
    return state_showing_match_waiting;
}
//OK
sm_states_t on_showing_match_timeout_expired(sm_struct_t *sm){
    LED_all_off();
    PRINT_MESSAGE_SIMPLE_U1("Ready for the next match??\n");
    return state_match_idle;
}

/**
  * @brief  Clears new event flag. Called every time a function is processed.
  * @param  <sm> pointer to state machine structure
  */
void  sm_clear_event(sm_struct_t *sm){
    sm->new_event = NO_EVENTS;
}

uint8_t sm_params_are_valid(sm_struct_t *sm){
    if (( sm->event < event_last) && (sm->state < state_last) && 
            (sm->new_event == NEW_EVENT) && (sm_callback_array[sm->state][sm->event]))
        return valid_params;
    else
        return invalid_params;
}

/**
  * @brief  Execution point of state machine. Must be constantly called.
  * @param  <sm> pointer to state machine structure
  */
void sm_exec(sm_struct_t *sm){
    if(sm_params_are_valid(sm)){
        sm_clear_event(sm);
        sm->state = sm_callback_array[sm->state][sm->event](sm);
    }
}

void sm_init(sm_struct_t *sm){    
    sm->state = state_boot;
    sm->timeout_counting_flag = 0;
    sm->timeout_counter = TIMEOUT_RESET_VALUE;
    sm->current_led = LED1;
    sm_set_event(sm, event_boot_ok);
    
    sm_callback_array [state_boot][event_boot_ok]               = on_boot_ok;
    sm_callback_array [state_match_idle][event_start_match_received]         = \
            on_start_match_received;
    sm_callback_array [state_set_idle][event_button_pressed]    = on_button_pressed;
    sm_callback_array [state_waiting_answers][event_answers_timeout_expired] = \
            on_answers_timeout_expired;
    sm_callback_array [state_waiting_answers][event_answers_received]        = \
            on_answers_received;
    sm_callback_array [state_set_decision][event_set_result_ok] = on_set_result_ok;
    sm_callback_array [state_showing_set_waiting][event_showing_set_timeout_expired]     = \
            on_showing_set_timeout_expired;
    sm_callback_array [state_set_decision][event_match_winner_ok]            = \
            on_match_winner_ok;
    sm_callback_array [state_showing_match_waiting][event_showing_match_timeout_expired] = \
            on_showing_match_timeout_expired;    
    
    
    single_game_decision_matrix[0][0] = 0;
    single_game_decision_matrix[0][1] = 1;
    single_game_decision_matrix[0][2] = 2;
    single_game_decision_matrix[1][0] = 2;
    single_game_decision_matrix[1][1] = 0;
    single_game_decision_matrix[1][2] = 1;
    single_game_decision_matrix[2][0] = 1;
    single_game_decision_matrix[2][1] = 2;
    single_game_decision_matrix[2][3] = 0;
}