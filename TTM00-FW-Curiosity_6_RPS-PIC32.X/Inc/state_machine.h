#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <stdint.h>

#define MAXIMUM_SETS 2

enum{
    NO_EVENTS,
    NEW_EVENT
};

enum{
    invalid_params,
    valid_params
};

enum{
    TIMEOUT_COUNTING_OFF,
    TIMEOUT_COUNTING_ON
};

enum{
    PAPER,
    ROCK,
    SCISSORS,
};

typedef enum{
    state_boot,
    state_match_idle,
    state_set_idle,
    state_waiting_answers,
    state_set_decision,
    state_showing_set_waiting,
    state_showing_match_waiting,
    state_last,                
}sm_states_t;

typedef enum{
    event_boot_ok,
    event_start_match_received,
    event_button_pressed, 
    event_answers_timeout_expired,
    event_answers_received,
    event_set_result_ok,
    event_showing_set_timeout_expired,
    event_match_winner_ok,
    event_showing_match_timeout_expired,
    event_last
}sm_events_t;

typedef struct{
    sm_states_t state;
    volatile sm_events_t event;
    volatile uint8_t new_event;
    volatile uint8_t timeout_counting_flag;
    volatile uint32_t timeout_counter; 
    uint8_t current_led; 
    uint8_t current_set_winner;
    uint8_t current_set;
    uint8_t player1_victories;
    uint8_t player2_victories;
    uint8_t match_winner;
}sm_struct_t;

extern sm_struct_t sm;

inline uint8_t sm_set_event(sm_struct_t *sm, sm_events_t event);
void sm_exec(sm_struct_t *sm);
inline void sm_decrease_counter(sm_struct_t *sm);
void sm_init(sm_struct_t *sm);

#endif /* STATE_MACHINE_H */