#ifndef _PIC32_UART_H_
#define	_PIC32_UART_H_

#include <stdint.h>

#ifndef SYS_FREQ
#define SYS_FREQ			(96000000UL)
#endif 
#ifndef PBCLK_FREQUENCY
#define PBCLK_FREQUENCY		(96 * 1000 * 1000)
#endif

#define PRINT_MESSAGE_U1(ARG1, ARG2)  sprintf(U1TxBuf, ARG1, ARG2);\
                    UART1_write_string(U1TxBuf)

#define PRINT_MESSAGE_SIMPLE_U1(ARG1)  sprintf(U1TxBuf, ARG1);\
                    UART1_write_string(U1TxBuf)

#define PRINT_MESSAGE_U2(ARG1, ARG2)  sprintf(U2TxBuf, ARG1, ARG2);\
                    UART2_write_string(U2TxBuf)

#define PRINT_MESSAGE_SIMPLE_U2(ARG1)  sprintf(U2TxBuf, ARG1);\
                    UART2_write_string(U2TxBuf)

#define PRINT_MESSAGE_U3(ARG1, ARG2)  sprintf(U3TxBuf, ARG1, ARG2);\
                    UART3_write_string(U3TxBuf)

#define PRINT_MESSAGE_SIMPLE_U3(ARG1)  sprintf(U3TxBuf, ARG1);\
                    UART3_write_string(U3TxBuf)

#define CLEAR_BUFF_TX1()    memset(U1TxBuf, 0, sizeof(U1TxBuf));
#define CLEAR_BUFF_RX1()    memset(U1RxBuf, 0, sizeof(U1RxBuf));
#define CLEAR_BUFF_RX2()    memset(U2RxBuf, 0, sizeof(U2RxBuf));
#define CLEAR_BUFF_TX2()    memset(U2TxBuf, 0, sizeof(U2TxBuf));
#define CLEAR_BUFF_RX3()    memset(U3RxBuf, 0, sizeof(U3RxBuf));
#define CLEAR_BUFF_TX3()    memset(U3TxBuf, 0, sizeof(U3TxBuf));

#define U1RxBufSize 64
#define U1TxBufSize 64
#define U2RxBufSize 64
#define U2TxBufSize 64
#define U3RxBufSize 64
#define U3TxBufSize 64

extern unsigned char U1TxBuf[U1TxBufSize];
extern unsigned char U1RxBuf[U1RxBufSize];
extern unsigned char U1Unread;
extern unsigned char U2TxBuf[U2TxBufSize];
extern unsigned char U2RxBuf[U2RxBufSize];
extern unsigned char U2Unread;
extern unsigned char U3TxBuf[U3TxBufSize];
extern unsigned char U3RxBuf[U3RxBufSize];
extern unsigned char U3Unread;

// UART1 functions
void UART1_init(unsigned long int UART_Baud);
void UART1_write_string(const char *string);

unsigned char UART1_is_unread(void);
void UART1_clear_unread(void);

uint8_t UART1_new_event();

// UART2 funtions
void UART2_init(unsigned long int UART_Baud);
void UART2_write_string(const char *string);

unsigned char UART2_is_unread(void);
void UART2_clear_unread(void);

// UART3 functions
void UART3_init(unsigned long int UART_Baud);
void UART3_write_string(const char *string);

unsigned char UART3_is_unread(void);
void UART3_clear_unread(void);


#endif	/* PIC32_UART_H_ */

