#include <xc.h>
#include "Inc/main.h"
#include "Inc/pic32_uart.h"
#include "Inc/led.h"
#include "plib.h"
#include "Inc/state_machine.h"
#include <stdint.h>


void board_setup(void){
    LED_setup();
    
    //UART1 pins
    // Set the F1 and F0 as Input/Output in order to be used by UART1
	mPORTFSetPinsDigitalIn(BIT_1);
	mPORTFSetPinsDigitalOut(BIT_0);
    
    // Set the B9 and E5 as Input/Output in order to be used by UART2
	mPORTBSetPinsDigitalIn(BIT_9);
    mPORTESetPinsDigitalOut(BIT_5);
    
    // Set the D3 and D2 as Input/Output in order to be used by UART2
	mPORTDSetPinsDigitalIn(BIT_3);
    mPORTDSetPinsDigitalOut(BIT_2);   
    
    UART1_init(UART_BAUD_RATE);  
    UART2_init(UART_BAUD_RATE);  
    UART3_init(UART_BAUD_RATE);  
    
    PRINT_MESSAGE_SIMPLE_U1("[PIC]: Initializing Board - U1\n\n");
    PRINT_MESSAGE_SIMPLE_U2("[PIC]: Initializing Board - U2\n\n");
    PRINT_MESSAGE_SIMPLE_U3("[PIC]: Initializing Board - U3\n\n");
    
    
    OpenCoreTimer(CORE_TICK_RATE);
    mConfigIntCoreTimer((CT_INT_ON | CT_INT_PRIOR_3 | CT_INT_SUB_PRIOR_0));
    // SW1 Initialize
    mCNDOpen(CND_ON | CND_IDLE_CON, CND6_ENABLE, CND_PULLUP_DISABLE_ALL);
    ConfigIntCND(CHANGE_INT_PRI_5 | CHANGE_INT_ON);
    
    /****************************** INTERRUPTS ***************************/
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();    
}

void main(){
    board_setup();   
    sm_init(&sm);
    while (1){
        if (UART1_new_event())
            sm_set_event(&sm, event_start_match_received);
        if (UART2_and_UART3_new_event())
            sm_set_event(&sm, event_answers_received);
        sm_exec(&sm);
    }
}

void __ISR(_CORE_TIMER_VECTOR, IPL3AUTO) CoreTimerHandler(void){
    mCTClearIntFlag();// clear the interrupt flag
    UpdateCoreTimer(CORE_TICK_RATE);// reload the period
    sm_decrease_counter(&sm);
}


void __ISR(_CHANGE_NOTICE_VECTOR, IPL5AUTO) change_notice_handler(void){
    mPORTDRead();
    if (!PORTDbits.RD6)
        sm_set_event(&sm,event_button_pressed);
    mCNDClearIntFlag();
}
